<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator" prefix="dec"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<c:set var="url" value="${pageContext.request.contextPath}"></c:set>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
    <%@ include file="../template/meta.jsp" %>

    <title>Home Page</title>
    <%@ include file="../template/header.jsp" %>
    <title>
        <dec:title default="Web Page" />
    </title>
    <dec:head />
</head>


<body>

    <%@ include file="../template/navBar.jsp" %>

    <dec:body />


    <div id="m_footer">
        <%@ include file="../template/footer.jsp" %>
    </div>
    <!-- /footer -->

</body>
</html>